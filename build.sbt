javacOptions in Compile ++= "-source" :: "1.7" :: "-target" :: "1.7" :: Nil

scalaVersion := "2.11.7"
scalacOptions in Compile += "-feature"

libraryDependencies += "org.scaloid" %% "scaloid" % "4.2"

updateCheck in Android := {} // disable update check
proguardCache in Android ++= Seq("org.scaloid", "scalaz")

proguardOptions in Android ++= Seq("-dontobfuscate", "-dontoptimize", "-keepattributes Signature", "-printseeds target/seeds.txt", "-printusage target/usage.txt"
  , "-dontwarn scala.collection.**" // required from Scala 2.11.4
  , "-dontwarn org.scaloid.**" // this can be omitted if current Android Build target is android-16
)

lazy val scajass = (project in file("lib/scajass"))
  .settings(android.Plugin.androidBuildAar: _*)
  .settings(platformTarget in Android := "android-16")
    .settings(updateCheck in Android := {})
     .settings(proguardCache in Android ++= Seq("scalaz.std"))

lazy val root = (project in file("."))
  .androidBuildWith(scajass)
  .dependsOn(scajass)