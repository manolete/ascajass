package manolete.aScaJass

import org.scaloid.common._
import android.graphics.Color

class chooseVariant extends SActivity {
  lazy val trumpfBtn = new SButton("Trumpf")

  onCreate {
    contentView = new SVerticalLayout {
      style {
        case b: SButton => b.textColor(Color.RED).onClick()
        case t: STextView => t textSize 10.dip
        case e: SEditText => e.backgroundColor(Color.YELLOW).textColor(Color.BLACK)
      }
      STextView("Choose Variant")
      trumpfBtn.here
    } padding 20.dip
  }
}